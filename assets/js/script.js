const searchBtn = document.querySelector('.search-btn');
const pageLogo = document.querySelector('.page-logo');
const navigation = document.querySelector('nav');
// const indexPageNews = document.querySelector('.index-page-news');
// const newsCard = document.querySelector('.home__news-card');

// const otherNewsBtn = document.querySelector('.other-news');

// otherNewsBtn.addEventListener('click', () => {
//     indexPageNews.innerHTML = '';
//     for (let i = 0; i < 9; i++) {
//         indexPageNews.appendChild(newsCard)
        
//     }
//     console.log(newsCard);
// })



searchBtn.addEventListener('click', () => {
    pageLogo.remove();
    searchBtn.remove();

    //Created and Added Search Input
    let input = document.createElement('input');
    input.className = 'header-input'
    input.placeholder = 'Axtarmaq istədiyiniz sözü daxil edin ...'
    navigation.appendChild(input);

    //Created and Added Cancel Btn
    let cancel = document.createElement('i');
    cancel.className = 'fas fa-times close-search-bar'
    navigation.appendChild(cancel);

    //Changed Style
    navigation.style.alignItems = 'center'

    //Restore All Properties 
    cancel.addEventListener('click', () => {
        input.remove();
        cancel.remove();
        navigation.append(pageLogo, searchBtn);  
        navigation.style.alignItems = 'flex-end'
    })
})